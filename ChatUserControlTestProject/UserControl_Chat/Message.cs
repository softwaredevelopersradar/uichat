﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserControl_Chat
{
    public class Message
    {
        public int Id { get; set; }
        public string MessageFiled { get; set; }

        //Locate message relative to the chat
        //SentByMe(0)-right side, Received(1)-left side, SendByAdmin(2) =  mid
        public Roles IsSendByMe { get; set; }

        //true-green tick, false-red cross
        public bool IsTransmited { get; set; }

        // sender name - station captcha
        public string SenderName { get; set; }
        public string ProfilePictureRGB { get; set; }
        public int MessageFontSize { get; set; }
        public int SenderNameFontSize { get; set; }
        public string BackgroundColor { get; set; }
        public string HexColorOfMessageFont { get; set; }

        public Message()
        {  
        }

        public Message( int id, string message, Roles isSendByMe, bool isTransmited, string senderName, string profilePictureRGB,
            int messageFontSize, int senderNameFontSize, string backgroundColor, string hexColorOfMessageFont)
        {
            this.Id = id;
            this.MessageFiled = message;
            this.IsSendByMe = isSendByMe;
            this.IsTransmited = isTransmited;
            this.SenderName = senderName;
            this.ProfilePictureRGB = profilePictureRGB;

            this.MessageFontSize = messageFontSize;
            this.SenderNameFontSize = senderNameFontSize;
            this.BackgroundColor = backgroundColor;
            this.HexColorOfMessageFont = hexColorOfMessageFont;


        }
    }
}
