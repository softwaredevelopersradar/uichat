﻿using System;
using System.Globalization;
using System.Windows;

namespace UserControl_Chat
{
    /// <summary>
    /// A converter that takes in a boolean and returns a <see cref="Visibility"/>
    /// </summary>
    public class BooleanToVisiblityConverter : BaseValueConverter<BooleanToVisiblityConverter>
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((Roles)value == Roles.Received)
                return Visibility.Visible;
            if ((Roles)value == Roles.SentByMe)
                return Visibility.Visible;
            if ((Roles)value == Roles.SendByAdmin)
                return Visibility.Hidden;
            else
                return Visibility.Hidden;

            //if (parameter == null)
            //    return (bool)value ? Visibility.Hidden : Visibility.Visible;
            //else
            //    return (bool)value ? Visibility.Visible : Visibility.Hidden;
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
