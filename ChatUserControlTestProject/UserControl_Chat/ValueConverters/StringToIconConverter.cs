﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Media;

namespace UserControl_Chat
{
    /// <summary>
    /// A converter that takes in an RGB string such as FF00FF and converts it to a WPF brush
    /// </summary>
    public class StringToIconConverter : BaseValueConverter<StringToIconConverter>
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var myResourceDictionary = new ResourceDictionary();
            myResourceDictionary.Source =
                new Uri("/UserControl_Chat;component/Styles/Fonts.xaml", UriKind.RelativeOrAbsolute);

            if(value == null)
            {
                return myResourceDictionary["FontAwesomeSenToOneTestClipIcon"];
            }
            else
            return myResourceDictionary[value];

        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
