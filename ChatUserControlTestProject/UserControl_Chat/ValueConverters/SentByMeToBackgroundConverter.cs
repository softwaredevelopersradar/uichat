﻿using System;
using System.Globalization;
using System.Windows;

namespace UserControl_Chat
{
    /// <summary>
    /// A converter that takes in a boolean if a message was sent by me, and returns the
    /// correct background color
    /// </summary>
    public class SentByMeToBackgroundConverter : BaseValueConverter<SentByMeToBackgroundConverter>
    {

        //TODO: fix problem with resource( without loading whole .xaml)
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {

            var myResourceDictionary = new ResourceDictionary();
            myResourceDictionary.Source =
                new Uri("/UserControl_Chat;component/Styles/Colors.xaml", UriKind.RelativeOrAbsolute);


            if ((Roles)value == Roles.SentByMe)
                return  myResourceDictionary["ForegroundLightBrush"];
            if ((Roles)value == Roles.Received)
                return myResourceDictionary["WordVeryLightBlueBrush"];
            if ((Roles)value == Roles.SendByAdmin)
                return myResourceDictionary["WordOrangeBrush"];
            else
                return (bool)value ? myResourceDictionary["WordVeryLightBlueBrush"] : myResourceDictionary["ForegroundLightBrush"];
            //return (bool)value ? Application.Current.FindResource("WordVeryLightBlueBrush") : Application.Current.FindResource("ForegroundLightBrush");
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
