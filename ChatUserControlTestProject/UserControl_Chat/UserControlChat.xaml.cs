﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UserControl_Chat
{


    /// <summary>
    /// Interaction logic for UserControlChat.xaml
    /// </summary>
    public partial class UserControlChat : UserControl
    {

        //ChatMessageListViewModel testCMLVM = new ChatMessageListViewModel();
        public int SideMenuWidth { get; set; }
        public int MinSideMenuWidth { get; set; }
        public int MaxSideMenuWidth { get; set; }
        public int DragIncrement { get; set; }

        
        public UserControlChat()
        {
            InitializeComponent();
            DataContext = this;
            this.SideMenuWidth = 55;
            this.MinSideMenuWidth = 3;
            this.MaxSideMenuWidth = 250;
            this.DragIncrement = 50;
        }

        public UserControlChat(int sideMenuWidth)
        {
            InitializeComponent();
            DataContext = this;
            this.SideMenuWidth = sideMenuWidth;
            this.MinSideMenuWidth = 5;
            this.MaxSideMenuWidth = 250;
            this.DragIncrement = 50;
        }

        public UserControlChat(int sideMenuWidth, int dragIncrement)
        {
            InitializeComponent();
            DataContext = this;
            this.SideMenuWidth = sideMenuWidth;
            this.MinSideMenuWidth = 5;
            this.MaxSideMenuWidth = 250;
            this.DragIncrement = dragIncrement;
        }

        public UserControlChat(int sideMenuWidth, int minSideMenuWidth, int maxSideMenuWidth)
        {
            InitializeComponent();
            DataContext = this;
            this.SideMenuWidth = sideMenuWidth;
            this.MinSideMenuWidth = minSideMenuWidth;
            this.MaxSideMenuWidth = maxSideMenuWidth;
        }

        public UserControlChat(int sideMenuWidth, int minSideMenuWidth, int maxSideMenuWidth, int dragIncrement)
        {
            InitializeComponent();
            DataContext = this;
            this.SideMenuWidth = sideMenuWidth;
            this.MinSideMenuWidth = minSideMenuWidth;
            this.MaxSideMenuWidth = maxSideMenuWidth;
            this.DragIncrement = dragIncrement;
        }

        private void ChatPage1_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Events.Click();
        }


        public void InitStations(List<StationClassForChat> listOfStations)
        {
            if (listOfStations.Count>0) 
                Events.Loaded(listOfStations[0].Id); // Loading chat with first member
            Events.GetlistOfChatMembers(listOfStations);
        }

        public void DrawMessageToChat(List<Message> stationsMessages)
        {     
            Events.DoActionWithMessage(stationsMessages);
        }

        public void DeleteChatMembers(List<int> listtoDelete)
        {
            Events.DeleteItemFromChat(listtoDelete) ;
        }

        public void ClearChatHistory(List<int> stations)
        {
            foreach (var station in stations)
            {
                Events.ClearChatStory(station);
            }
        }

        //public void ClearAllChats()
        //{
        //    foreach (var station in stations)
        //    {
        //        Events.ClearChatStory(station);
        //    }
        //}

        public void AddChatMembers(List<StationClassForChat> listToAdd)
        {
            Events.AddItemToChat(listToAdd);
        }

        public void DrawMessageToChatAsAdmin(List<Message> stationsMessages)
        {
            Events.DoAdminActionWithMessage(stationsMessages);
        }

        public void ConfirmMessage(Message stationsMessages)
        {
            Events.DoConfirmMessage(stationsMessages);
        }

        public void SetSideMenuWidth(int sideMenuWidth)
        {
            this.SideMenuWidth = sideMenuWidth; 
        }

        public void SetMinSideMenuWidth(int minsideMenuWidth)
        {
            this.MinSideMenuWidth = minsideMenuWidth;
        }

        public void SetMaxSideMenuWidth(int maxsideMenuWidth)
        {
            this.MaxSideMenuWidth = maxsideMenuWidth;
        }

        public void UpdateSideMenuMembers(List<StationClassForChat> listToUpdate)
        {
            Events.AddItemToChat(listToUpdate);
        }
    }
}
