﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserControl_Chat;

namespace UserControl_Chat
{
    public static class Events
    {
        public delegate void EmptyEventHandler();
        public static event EmptyEventHandler OnLoadChatStory;
        public static event EmptyEventHandler OnSaveChatStory;
        public static event EmptyEventHandler OnClick;
        public static event EmptyEventHandler OnGetTopByScrollBar;
        public static event EmptyEventHandler OnClosingChat;
        public static event EmptyEventHandler OnUserNotAviableAnymore;


        public delegate void IntEventHandler(int id);
        public static event IntEventHandler OnLoaded;
        public static event IntEventHandler OnSelectedItem;
        public static event IntEventHandler OnClearChatStory;

        public delegate void StringEventHandler(string senderName);
        public static event StringEventHandler OnChangeTitle;


        public delegate void OperationWithStationIdDeleteEventHandler(List<int> listOfChatMembersToDelete);
        public static event OperationWithStationIdDeleteEventHandler OnDeleteItemFromChat;
        public static event OperationWithStationIdDeleteEventHandler OnUpdateChatItems;


        public delegate void MessageEventHandler(List<Message> stationsMessages);
        public static event MessageEventHandler OnSendStationsMessage;
        public static event MessageEventHandler OnGetStationsMessage;
        public static event MessageEventHandler OnDoActionWithMessage;
        public static event MessageEventHandler OnDoAdminActionWithMessage;

        public delegate void OneMessageEventHandler(Message stationsMessages);
        public static event OneMessageEventHandler OnDoConfirmMessage;

        public delegate void StationEventHandler(List<StationClassForChat> listOfChatMembers);
        public static event StationEventHandler OnGetlistOfChatMembers;
        public static event StationEventHandler OnAddItemToChat;

        public static void LoadChatStory()
        {
            if (OnLoadChatStory != null)
            {
                OnLoadChatStory();
            }
        }

        public static void SaveChatStory()
        {
            if (OnSaveChatStory != null)
            {
                OnSaveChatStory();
            }
        }

        public static void Click()
        {
            if (OnClick != null)
            {
                OnClick();
            }
        }

        public static void GetTopByScrollBar()
        {
            if (OnGetTopByScrollBar != null)
            {
                OnGetTopByScrollBar();
            }
        }

        public static void Loaded(int idOrNumber)
        {
            if (OnLoaded != null)
            {
                OnLoaded(idOrNumber);
            }
        }

        public static void SelectedItem(int idOrNumber)
        {
            if (OnSelectedItem != null)
            {
                OnSelectedItem(idOrNumber);
            }
        }

        public static void ClearChatStory(int idOrNumber)
        {
            if (OnClearChatStory != null)
            {
                OnClearChatStory(idOrNumber);
            }
        }

        public static void GetlistOfChatMembers(List<StationClassForChat> listOfChatMembers)
        {
            if (OnGetlistOfChatMembers != null)
            {
                OnGetlistOfChatMembers(listOfChatMembers);
            }
        }



        public static void AddItemToChat(List<StationClassForChat> listOfChatMembers)
        {
            if (OnAddItemToChat != null)
                OnAddItemToChat(listOfChatMembers);
        }

        public static void DeleteItemFromChat(List<int> listOfChatMembersToDelete)
        {
            if (OnDeleteItemFromChat != null)
                OnDeleteItemFromChat(listOfChatMembersToDelete);
        }

        public static void UpdateChatItems(List<int> listOfChatMembersToDelete)
        {
            if (OnUpdateChatItems != null)
                OnUpdateChatItems(listOfChatMembersToDelete);
        }

        public static void SendStationsMessage(List<Message> stationsMessages)
        {
            if (OnSendStationsMessage != null)
                OnSendStationsMessage(stationsMessages);
        }

        public static void GetStationsMessage(List<Message> stationsMessages)
        {
            if (OnGetStationsMessage != null)
                OnGetStationsMessage(stationsMessages);
        }

        public static void DoActionWithMessage(List<Message> stationsMessages)
        {
            if (OnDoActionWithMessage != null)
                OnDoActionWithMessage(stationsMessages);
        }

        public static void DoAdminActionWithMessage(List<Message> stationsMessages)
        {
            if (OnDoAdminActionWithMessage != null)
                OnDoAdminActionWithMessage(stationsMessages);
        }

        public static void DoConfirmMessage(Message stationsMessages)
        {
            if (OnDoConfirmMessage != null)
                OnDoConfirmMessage(stationsMessages);
        }

        public static void ChangeTitle(string senderName)
        {
            if (OnChangeTitle != null)
            {
                OnChangeTitle(senderName);
            }
        }

        public static void ClosingChat()
        {
            if (OnClosingChat != null)
            {
                OnClosingChat();
            }
        }

        public static void UserNotAviableAnymore()
        {
            if (OnUserNotAviableAnymore != null)
            {
                OnUserNotAviableAnymore();
            }
        }
    }
}
