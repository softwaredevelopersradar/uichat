﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UserControl_Chat
{
    /// <summary>
    /// Interaction logic for ChatPage.xaml
    /// </summary>
    public partial class ChatPage : UserControl
    {
        bool StopLoading = false; // This is required to ensure that only the specified amount of messages will be loaded at once.
                                  // If we scroll upper the messages, then don't scroll. If we scroll to bottom, scroll the messages
        private int curIdOfStation;
        private List<StationClassForChat> curlistOfStationMembers;

        //public event EventHandler<int> OnClearChat;

        public ChatPage()
        {
            InitializeComponent();
            DataContext = new ChatMessageListViewModel();
            Events.OnLoaded += Change_Title;
            Events.OnSelectedItem += Change_Title;
            Events.OnDoActionWithMessage += TestCrutch;

            //TestPart
            Events.OnGetlistOfChatMembers += LoadSideMenu;
            Events.OnDeleteItemFromChat += ItemsReMover;
            Events.OnAddItemToChat += ItemAdder;
        }

        private void TestCrutch(List<Message> stationsMessages)
        {
            stationsMessages.ForEach(item =>
            {
                if (item.Id == curIdOfStation)
                    this.Scroll.ScrollToEnd();
            });
        }

        private void Change_Title(int idOfInterlocutor)
        {
            //if (idOfInterlocutor == 0)
            //{
            //    TitleOfChatPage.Text = "ПУ";// 0 is always for ПУ
            //}
            //else
            //{
            //    TitleOfChatPage.Text = idOfInterlocutor.ToString();
            //}
            if (curlistOfStationMembers != null)
            {
                curlistOfStationMembers.ForEach(item =>
                {
                    if (item.Id == idOfInterlocutor)
                    {
                        TitleOfChatPage.Text = item.Caption;
                    }
                });
            }

            curIdOfStation = idOfInterlocutor;
            StopLoading = false;
            //this.Scroll.ScrollToEnd();
        }


        //TODO: make normal scroll + style
        private void Scroll_fix(object sender, RoutedEventArgs e)
        {
            this.Scroll.ScrollToEnd();
        }


        private void MessageScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            // The scrollbar is at the top
            if (!StopLoading && Scroll.VerticalOffset == 0 && 
                Scroll.ComputedVerticalScrollBarVisibility == Visibility.Visible)// Load older messages
            {
                Events.GetTopByScrollBar();
                StopLoading = true;
            }
        }



        public void LoadSideMenu(List<StationClassForChat> listOfChatMembers)
        {
            curlistOfStationMembers = listOfChatMembers;
        }

        private void ItemsReMover(List<int> listOfIdToDelete)
        {
            listOfIdToDelete.ForEach(chatMemberId =>
            {
                curlistOfStationMembers.ToList().ForEach(item =>
                {
                    if (item.Id == chatMemberId)
                    {
                        curlistOfStationMembers.Remove(item);
                    }
                });
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="listToAdd"></param>
        private void ItemAdder(List<StationClassForChat> listToAdd)
        {

            listToAdd.ForEach(stationMamber =>
            {
                curlistOfStationMembers.Add(stationMamber);
            });
        }

        private void ButtonDelete_OnClick(object sender, RoutedEventArgs e)
        {
            Events.ClearChatStory(this.curIdOfStation);
        }
    }
}
