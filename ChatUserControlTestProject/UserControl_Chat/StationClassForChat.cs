﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserControl_Chat
{
    public class StationClassForChat
    {
        public int Id { get; set; }
        public string Caption { get; set; }
        public bool IsActive { get; set; }

        public StationClassForChat()
        {
        }

        public StationClassForChat(int Id, string caption, bool isActive)
        {
            this.Id = Id;
            this.Caption = caption;
            this.IsActive = isActive;
        }


    }
}
