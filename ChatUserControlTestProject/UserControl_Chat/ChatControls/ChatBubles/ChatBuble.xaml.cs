﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UserControl_Chat
{
    /// <summary>
    /// Interaction logic for ChatBuble.xaml
    /// </summary>
    public partial class ChatBuble : UserControl
    {
        public ChatBuble()
        {
            InitializeComponent();
        }

        private void TextBlock_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                string ct = ((sender as TextBlock).Text);
                Clipboard.SetText(ct);
            }
            catch (Exception ex) { }
        }
    }
}
