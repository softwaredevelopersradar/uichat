﻿
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace UserControl_Chat
{
    /// <summary>
    /// The design-time data for a <see cref="ChatMessageListViewModel"/>
    /// </summary>
    public class ChatMessageListDesignModel : ChatMessageListViewModel
    {
        #region Singleton
        /// <summary>
        /// A single instance of the design model
        /// </summary>
        public static ChatMessageListDesignModel Instance => new ChatMessageListDesignModel();

        #endregion

        #region Constructor

        public ChatMessageListDesignModel()
        {
            if (Items == null)
                Items = new ObservableCollection<ChatMessageListItemViewModel>
            {
                new ChatMessageListItemViewModel
                {
                    Message = "Let me know when you manage to spin up the new 2016 server",
                    MessageSentTime = DateTimeOffset.UtcNow,
                    SentByMe = Roles.SentByMe,
                    State = "Sending is Failed",
                    ColorOfIcon = "Red",
                    Icon = "test5"
                },
                new ChatMessageListItemViewModel
                {
                    Message = "Let me know when you manage to spin up the new 2016 server",
                    MessageSentTime = DateTimeOffset.UtcNow,
                    SentByMe = Roles.SentByMe,
                    State = "Sending is Failed",
                    ColorOfIcon = "Red",
                    Icon = "test5"
                },
                new ChatMessageListItemViewModel
                {
                    Message = "Let me know when you manage to spin up the new 2016 server",
                    MessageSentTime = DateTimeOffset.UtcNow,
                    SentByMe = Roles.SentByMe,
                    State = "Sending is Failed",
                    ColorOfIcon = "Red",
                    Icon = "test5"
                },

            };
        }

        #endregion
    }
}
