﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using MessageBox = System.Windows.Forms.MessageBox;

namespace UserControl_Chat
{

    /// <summary>
    /// A view model for a chat message thread  list 
    /// </summary>
    [ProtoContract]
    public class ChatMessageListViewModel : BaseViewModel
    {
        private string nameOfBinFile = "chatData.bin";
        private int oldIdOfStation = -1;
        private int curIdOfStation;

        /// <summary>
        /// The text for the current message being written
        /// </summary>
        public string PendingMessageText { get; set; }
        public string Icon { get; set; }

        private Dictionary<int, ObservableCollection<ChatMessageListItemViewModel>> dictionaryOfChats = new Dictionary<int, ObservableCollection<ChatMessageListItemViewModel>>();
        private List<int> localListOfChatMembers = new List<int>();
        private int counter = 1;

        // TEST//
        private Dictionary<int, ObservableCollection<ChatMessageListItemViewModel>> curDictionaryOfChats = new Dictionary<int, ObservableCollection<ChatMessageListItemViewModel>>();
        private int numberOfBublesToDisplay = 10;

        #region Commands


        public ObservableCollection<ChatMessageListItemViewModel> Items { get; set; }
        public ICommand SendCommand { get; set; }
        public ICommand SaveChatStoryToTXTCommand { get; set; }
        public ICommand LoadCommand { get; set; }
        public ICommand SaveCommand { get; set; }
        public ICommand DeleteCommand { get; set; }
        public ICommand ChangeIconCommand { get; set; }

        public ICommand CloseChat { get; set; }

        #endregion

        #region Constructor
        public ChatMessageListViewModel()
        {
            SendCommand = new RelayCommand(Send);
            SaveChatStoryToTXTCommand = new RelayCommand(SaveChatStoryToTXT);
            LoadCommand = new RelayCommand(LoadChatStory);
            SaveCommand = new RelayCommand(SaveChatStory);
            DeleteCommand = new RelayCommand(Delete);
            ChangeIconCommand = new RelayCommand(ChangeIcon);

            CloseChat = new RelayCommand(CloseChatMethod);

            Events.OnSelectedItem += ChangeSideMenuElement;

            //TODO: choose way to load sideMenu element
            Events.OnGetlistOfChatMembers += LoadSideMenuElementsToLocalList;

            Events.OnUpdateChatItems += UpDateSideMenuElements;

            // load first item from SideMenu after loading
            Events.OnLoaded += ChangeSideMenuElement;


            Events.OnLoadChatStory += LoadChatStory;

            Events.OnSaveChatStory += SaveChatStory;

            Events.OnGetTopByScrollBar += LoadWholeChatStory;
            Events.OnDoActionWithMessage += DrawBublesToChat;

            Events.OnDoConfirmMessage += ConfirmSentMessage;

            // this station was deleted from tableASP -> chat not aviable and we  are droving system message about that
            Events.OnUserNotAviableAnymore += DrawSystemMessage;
            Events.OnClearChatStory += ClearChatStory;
        }

        private void ClearChatStory(int id)
        {
            if (id == curIdOfStation)
                Delete();
        }



        #endregion

        //TODO: make description for functions
        #region Loading Members and so on

        private void ChangeSideMenuElement(int chatMemberId)
        {
            //curIdOfStation = chatMemberId;
            OpenNewBubleChat(chatMemberId);
        }

        private void OpenNewBubleChat(int idOfStation)
        {
            if (curIdOfStation != idOfStation)
            {
                ObservableCollection<ChatMessageListItemViewModel> OldItems = new ObservableCollection<ChatMessageListItemViewModel>();
                if (dictionaryOfChats.ContainsKey(idOfStation))
                {
                    //TODO: if tems= null avoid action
                    OldItems = dictionaryOfChats[idOfStation];
                }
                else
                {
                    dictionaryOfChats.Add(idOfStation, Items);

                }

                Items = OldItems;
                oldIdOfStation = idOfStation;
            }
            curIdOfStation = idOfStation;
        }

        private void LoadSideMenuElementsToLocalList(List<StationClassForChat> listOfChatMembers)
        {
            localListOfChatMembers = listOfChatMembers.Select(item => item.Id).ToList();

            //TODO: remove it with better decision
            foreach (int chatMemberId in localListOfChatMembers)
            {
                dictionaryOfChats[chatMemberId] = new ObservableCollection<ChatMessageListItemViewModel>();
                curDictionaryOfChats[chatMemberId] = new ObservableCollection<ChatMessageListItemViewModel>();
            }
        }

        private void UpDateSideMenuElements(List<int> listOfChatMembers)
        {
            localListOfChatMembers = listOfChatMembers;

            //TODO: remove it with better decision
            foreach (int chatMemberId in localListOfChatMembers)
            {
                if (!dictionaryOfChats.ContainsKey(chatMemberId))
                {
                    dictionaryOfChats[chatMemberId] = new ObservableCollection<ChatMessageListItemViewModel>();
                    curDictionaryOfChats[chatMemberId] = new ObservableCollection<ChatMessageListItemViewModel>();
                }
            }
        }

        #endregion

        #region AllAboutSendingMessages

        public void ChangeIcon()
        {
            Icon = counter % 2 != 0 ? "FontAwesomeSenToAllClipIcon" : "FontAwesomeSenToOneTestClipIcon";
            counter++;
        }

        public void CloseChatMethod()
        {
            Events.ClosingChat();
        }

        public void Send()
        {
            if (!string.IsNullOrWhiteSpace(PendingMessageText))
            {
                List<Message> stationsMessages = new List<Message>();
                bool toSingle2 = counter % 2 != 0 ? true : false;
                //Do i need new element?

                if (toSingle2)
                {
                    Message curMessage = new Message()
                    {
                        Id = curIdOfStation,
                        MessageFiled = PendingMessageText,
                        IsSendByMe = Roles.SentByMe
                    };
                    stationsMessages.Add(curMessage);
                }
                else
                {
                    foreach (int chatMember in localListOfChatMembers)
                    {
                        Message curMessage = new Message()
                        {
                            Id = chatMember,
                            MessageFiled = PendingMessageText,
                            IsSendByMe = Roles.SentByMe
                        };
                        stationsMessages.Add(curMessage);
                    }
                }

                PendingMessageText = "";
                Events.SendStationsMessage(stationsMessages);
            }
        }


        /// <summary>
        /// is edding last message in chat
        /// </summary>
        /// <param name="stationsMessages"></param>
        private void ConfirmSentMessage(Message stationMessage)
        {
            stationMessage.IsTransmited = true;

            if (!localListOfChatMembers.Contains(stationMessage.Id))
                return;

            //Message curMessage = stationsMessages.FirstOrDefault(item => item.Id == chatMember);
            Items = dictionaryOfChats[stationMessage.Id];
            if (Items == null)
                return;

            var findedItem = Items.Last();

            Items.Remove(findedItem);

            findedItem.State = "Successfully sent";
            findedItem.ColorOfIcon = "GreenYellow";
            findedItem.Icon = "FontAwesomeTickIcon";

            Items.Add(findedItem);

            if (dictionaryOfChats[curIdOfStation] != null && dictionaryOfChats[curIdOfStation].Count != 0)
                MethodToFillBuffer((dictionaryOfChats[curIdOfStation])[dictionaryOfChats[curIdOfStation].Count - 1]);
            dictionaryOfChats[stationMessage.Id] = Items;
            
            Items = MethodToLoadCuttedChatStory(dictionaryOfChats[curIdOfStation]);
        }

        /// <summary>
        /// is edding messages to chat
        /// </summary>
        /// <param name="stationsMessages"></param>
        public void DrawBublesToChat(List<Message> stationsMessages)
        {
            foreach (int chatMember in localListOfChatMembers)
            {
                Items = null;
                foreach (Message stationsMessage in stationsMessages)
                {
                    //if (stationsMessages.Select(item => item.Id).Contains(chatMember))
                    if (stationsMessage.Id == chatMember)
                    {
                        //Message curMessage = stationsMessages.FirstOrDefault(item => item.Id == chatMember);
                        Items = dictionaryOfChats[chatMember];
                        if (Items == null)
                            Items = new ObservableCollection<ChatMessageListItemViewModel>();
                        if (!string.IsNullOrWhiteSpace(stationsMessage.MessageFiled))
                        {
                            Items.Add(
                            new ChatMessageListItemViewModel
                            {
                                //Message = String.Format("{0} : {1}", stationsMessage.Id, stationsMessage.MessageFiled),
                                Message = stationsMessage.MessageFiled,
                                MessageSentTime = DateTimeOffset.UtcNow,
                                SentByMe = stationsMessage.IsSendByMe,
                                State = stationsMessage.IsTransmited == true ? "Successfully sent" : "Sending is Failed",
                                ColorOfIcon = stationsMessage.IsTransmited == true ? "GreenYellow" : "Red",
                                Icon = stationsMessage.IsTransmited == true ? "FontAwesomeTickIcon" : "FontAwesomeCrossIcon",
                                ProfilePictureRGB = stationsMessage.ProfilePictureRGB == null ? "FFFFFF" : stationsMessage.ProfilePictureRGB,
                                SenderName = stationsMessage.SenderName == null ? "" : stationsMessage.SenderName,
                                Id = stationsMessage.Id,

                                MessageFontSize = stationsMessage.MessageFontSize == 0 ? 24 : stationsMessage.MessageFontSize,
                                SenderNameFontSize = stationsMessage.SenderNameFontSize == 0 ? 23 : stationsMessage.SenderNameFontSize,
                                BackgroundColor = stationsMessage.BackgroundColor == null ? "ffa800" : stationsMessage.BackgroundColor,
                                MarginForSenderName = String.Format("33 {0} 33 0", stationsMessage.SenderNameFontSize*(-0.8)),
                                MarginForMarginForBubleGrid = String.Format("25 10 25 {0}", stationsMessage.SenderNameFontSize * (0.2)),
                                HexColorOfMessageFont = stationsMessage.HexColorOfMessageFont == null ? "FFFFFF" : stationsMessage.HexColorOfMessageFont,
                            }
                            );

                            //sending last elemenet from cur Items
                            //ObservableCollection<ChatMessageListItemViewModel> test_1 = dictionaryOfChats[curIdOfStation];
                            //ChatMessageListItemViewModel test_4 = test_1[test_1.Count - 1];

                            if (dictionaryOfChats[curIdOfStation] != null && dictionaryOfChats[curIdOfStation].Count != 0)
                                MethodToFillBuffer((dictionaryOfChats[curIdOfStation])[dictionaryOfChats[curIdOfStation].Count - 1]);
                            dictionaryOfChats[chatMember] = Items;
                        }
                    }
                }
            }
            Items = MethodToLoadCuttedChatStory(dictionaryOfChats[curIdOfStation]);
            //Items = dictionaryOfChats[curIdOfStation];
        }

        private void MethodToFillBuffer(ChatMessageListItemViewModel item)
        {
            if (curDictionaryOfChats[curIdOfStation] == null)
                curDictionaryOfChats[curIdOfStation] = new ObservableCollection<ChatMessageListItemViewModel>();
            curDictionaryOfChats[curIdOfStation].Add(item);
        }

        private void LoadWholeChatStory()
        {
            Items = curDictionaryOfChats[curIdOfStation];
        }

        private ObservableCollection<ChatMessageListItemViewModel> MethodToLoadCuttedChatStory(ObservableCollection<ChatMessageListItemViewModel> items)
        {
            if (items.Count > numberOfBublesToDisplay)
            {
                ObservableCollection<ChatMessageListItemViewModel> bufferCollection = new ObservableCollection<ChatMessageListItemViewModel>();
                bufferCollection = items;
                while (bufferCollection.Count > numberOfBublesToDisplay)
                {
                    bufferCollection.RemoveAt(0);
                }
                return bufferCollection;
            }
            else
            {
                return items;
            }
        }

        #endregion

        private void DrawSystemMessage()
        {
            List<Message> systemMessage = new List<Message>();
            Message testName = new Message();
            testName.Id = curIdOfStation;
            testName.MessageFiled = "Данный пользователь больше не доступен." + "\r\n" 
                + "Выберите любого доступного пользователя" + "\r\n" + "для продолжения работы.";
            testName.IsSendByMe = Roles.SendByAdmin;
            testName.IsTransmited = true;
            testName.MessageFontSize = 27;
            testName.HexColorOfMessageFont = "E51616";
            systemMessage.Add(testName);

            DrawBublesToChat(systemMessage);
        }


        #region clear func
        public void Delete()
        {
            Items = new ObservableCollection<ChatMessageListItemViewModel>();
            dictionaryOfChats[curIdOfStation] = new ObservableCollection<ChatMessageListItemViewModel>();
            curDictionaryOfChats[curIdOfStation] = new ObservableCollection<ChatMessageListItemViewModel>();
            PendingMessageText = "";
        }

        #endregion

        #region Save\Load\Start\End

        private void SaveChatStoryToTXT()
        {
            try
            {
                // Configure save file dialog box
                Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
                dlg.FileName = "Document"; // Default file name
                dlg.DefaultExt = ".txt"; // Default file extension
                dlg.Filter = "Text documents (.txt)|*.txt"; // Filter files by extension

                // Show save file dialog box
                if (dlg.ShowDialog() == true)
                {

                    StringBuilder sb = new StringBuilder();
                    //TODO: remove it with string builder

                    foreach (int dictionaryKey in dictionaryOfChats.Keys.ToList())
                    {
                        Items = dictionaryOfChats[dictionaryKey];
                        sb.Append(dictionaryKey + "\r\n");
                        foreach (ChatMessageListItemViewModel Item in Items)
                        {
                            sb.Append("Message: " + Item.Message + " ");
                            sb.Append(Item.SentByMe.ToString() + " ");


                            // Process save file dialog box results

                            // Save document
                            string filename = dlg.FileName;
                            using (StreamWriter sw = new StreamWriter(filename, true, System.Text.Encoding.UTF8))
                            {
                                sw.WriteLine("");
                                sw.WriteLine(sb);
                            }
                            sb.Clear();
                        }
                    }
                }
            }
            catch
            {
                
            }
        }

        public void SaveChatStory()
        {
            try
            {
                SaveFileDialog sfd = new SaveFileDialog();
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    string path = Path.GetFullPath(sfd.FileName);
                    if (!path.EndsWith(".bin"))
                        path += ".bin";

                    string finalPath = path == null ? nameOfBinFile : path;
                    using (var file = File.Create(finalPath))
                    {
                        Serializer.Serialize(file, dictionaryOfChats);
                    }
                }
            }
            catch { }
        }

        public void LoadChatStory()
        {
            try
            {

                OpenFileDialog ofd = new OpenFileDialog();
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    string path = Path.GetFullPath(ofd.FileName);
                    Dictionary<int, ObservableCollection<ChatMessageListItemViewModel>> oldDictionaryOfChats;
                    using (var file = File.OpenRead(path))
                    {
                        oldDictionaryOfChats = Serializer.Deserialize<Dictionary<int, ObservableCollection<ChatMessageListItemViewModel>>>(file);
                    }
                    dictionaryOfChats = oldDictionaryOfChats;

                    if (localListOfChatMembers.Any())
                    {
                        if (oldIdOfStation != -1)
                        {
                            OpenNewBubleChat(oldIdOfStation);
                        }
                        else
                        {
                            OpenNewBubleChat(localListOfChatMembers[0]);
                        }
                    }
                }

                Items = dictionaryOfChats[curIdOfStation];
            }
            catch
            {
                MessageBox.Show("Data is corrupted");
            }
        }
        #endregion


    }
}
