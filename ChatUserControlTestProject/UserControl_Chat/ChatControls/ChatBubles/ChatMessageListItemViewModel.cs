﻿using ProtoBuf;
using System;

namespace UserControl_Chat
{
    public enum Roles
    {
        SentByMe = 0,
        Received = 1,
        SendByAdmin = 2
    }
    /// <summary>
    /// A view model for each chat message thread item in the chat thread
    /// </summary>
    [ProtoContract]
    public class ChatMessageListItemViewModel : BaseViewModel
    {
        //public static new ChatMessageListItemViewModel Instance => new ChatMessageListItemViewModel();
        /// <summary>
        /// The display name of the sender of the message
        /// </summary>
        [ProtoMember(5)]
        public string SenderName { get; set; }

        /// <summary>
        /// The latest message from this chat
        /// </summary>
        [ProtoMember(2)]
        public string Message { get; set; }

        /// <summary>
        /// The initials to show for the profile picture background
        /// </summary>
        [ProtoMember(1)]
        public string Initials { get; set; }

        /// <summary>
        /// The RGB values (in hex) for the background color of the profile picture
        /// For example FF00FF for Red and Blue mixed
        /// </summary>
        [ProtoMember(3)]
        public string ProfilePictureRGB { get; set; }

        /// <summary>
        /// True if this item is currently selected
        /// </summary>
        [ProtoMember(6)]
        public bool IsSelected { get; set; }

        /// <summary>
        /// True if this message was sent by  the signed user
        /// </summary>
        [ProtoMember(7)]
        public Roles SentByMe { get; set; }

        [ProtoMember(8)]
        public string Icon { get; set; }

        [ProtoMember(9)]
        public string ColorOfIcon { get; set; }

        [ProtoMember(10)]
        public string State { get; set; }

        [ProtoMember(11)]
        public int Id { get; set; }

        [ProtoMember(13)]
        public int MessageFontSize { get; set; }

        [ProtoMember(14)]
        public int SenderNameFontSize { get; set; }

        [ProtoMember(15)]
        public string BackgroundColor { get; set; }

        [ProtoMember(16)]
        public string MarginForSenderName { get; set; }

        [ProtoMember(17)]
        public string MarginForMarginForBubleGrid { get; set; }

        [ProtoMember(18)]
        public string HexColorOfMessageFont { get; set; }


        /// <summary>
        /// The time  the message was sent
        /// </summary>
        [ProtoMember(4)]
        public DateTimeOffsetSurrogate MessageSentTime { get; set; }
    }

    [ProtoContract]
    public class DateTimeOffsetSurrogate
    {
        [ProtoMember(12)]
        public string DateTimeString { get; set; }

        public static implicit operator DateTimeOffsetSurrogate(DateTimeOffset value)
        {
            return new DateTimeOffsetSurrogate { DateTimeString = value.ToString("u") };
        }

        public static implicit operator DateTimeOffset(DateTimeOffsetSurrogate value)
        {
            return DateTimeOffset.Parse(value.DateTimeString);
        }
    }
}
