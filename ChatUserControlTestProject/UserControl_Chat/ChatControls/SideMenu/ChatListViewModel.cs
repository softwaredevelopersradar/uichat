﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace UserControl_Chat
{
    /// <summary>
    /// A view model for the overview chat list 
    /// </summary>
    public class ChatListViewModel : BaseViewModel
    {
        //public static ChatListViewModel Instance => new ChatListViewModel();

        public ICommand OpenMessageCommand { get; set; }

        #region variables
        /// <summary>
        /// The members of the chat
        /// </summary>
        public static ObservableCollection<ChatListItemViewModel> SideMenuItems { get; set; }
        private List<int> localListOfChatMembers;
        private static Random rm = new Random();
        private int prevIdOfStation = -1;
        private int curIdOfStation;

        //private string grayColor = "333333";
        private string grayColor = "0c6991";
        private string blueColor = "45b6e5";
        private string blackColor = "111111";
        private int startIndexOfList = 0;
        private byte counterForRandomColor = 14;


        //random colors from time to time was to ugly, so this 13 is almost fine.
        private List<string> curListOfColors = new List<string>(new string[] { "133337", "065535", "74809d", "005a5c", "336600",
            "6666ff",  "003366", "3399ff", "0099cc", "993333", "ff9900", "996600", "ffcc00",
            "ff6633", "ffff00"});

        private Dictionary<int, string> curColors;

        #endregion

        public ChatListViewModel()
        {
            curColors = new Dictionary<int, string>();

            Events.OnGetlistOfChatMembers += LoadSideMenu;

            //Add blue strip to new received messages(unread)
            Events.OnDoActionWithMessage += RecolorReceivedMessagesToChat;

            Events.OnSelectedItem += ReColorSelectedItem;

            Events.OnDeleteItemFromChat += RemoveItems;
            // Events.OnAddItemToChat += AddItems;
            Events.OnAddItemToChat += UpdateItems;
        }



        #region Initialization
        public void LoadSideMenu(List<StationClassForChat> listOfChatMembers)
        {
            localListOfChatMembers = listOfChatMembers.Select(item => item.Id).ToList();
            InitlistOfChatMembers(listOfChatMembers);

        }


        public void InitlistOfChatMembers(List<StationClassForChat> listOfChatMembers)
        {
            if (SideMenuItems == null)
                SideMenuItems = new ObservableCollection<ChatListItemViewModel>();

            //if (!string.IsNullOrEmpty(PendingMessageText))
            for (int i = 0; i < listOfChatMembers.Count; i++)
            {
                if (i == 0)// drow first item of chat as choosen item(make it better)
                {
                    SideMenuItems.Add(
                    new ChatListItemViewModel
                    {
                        Id = listOfChatMembers[i].Id,
                        Name = listOfChatMembers[i].Caption,
                        //Message = station.Value.Trim('\r','\n'),
                        ProfilePictureRGB = RandomRGBColor(),
                        NewContentAvailableField = grayColor,
                        BackgrounField = grayColor
                    });
                    curIdOfStation = listOfChatMembers[i].Id;
                }
                else
                {
                    SideMenuItems.Add(
                    new ChatListItemViewModel
                    {
                        Id = listOfChatMembers[i].Id,
                        Name = listOfChatMembers[i].Caption,
                        //Message = station.Value.Trim('\r','\n'),
                        ProfilePictureRGB = RandomRGBColor(),
                        //NewContentAvailable = true,
                        //IsSelected = true,
                        NewContentAvailableField = blackColor,
                        BackgrounField = blackColor
                    });
                }
            }

        }
        #endregion


        #region Recolorers

        private void ReColorSelectedItem(int curStationId)
        {
            prevIdOfStation = curIdOfStation;
            curIdOfStation = curStationId;

            if (prevIdOfStation == -1)
            {
                //non-executable code? (Haven't time to tast it, but seems like that part(if part) is non-executable after all refactoring)
                ChatSingleItemsRecolorer(curStationId, grayColor, blueColor);
            }
            else
            {
                ChatSingleItemsRecolorer(prevIdOfStation, blackColor, blackColor);
                ChatSingleItemsRecolorer(curStationId, grayColor, grayColor);
            }

        }

        public void RecolorReceivedMessagesToChat(List<Message> stationsMessages)
        {
            foreach (int chatMember in localListOfChatMembers)
            {
                foreach (Message station in stationsMessages)
                {
                    //if (!station.IsSendByMe && station.Id == chatMember)
                    if ((station.IsSendByMe != Roles.SentByMe) && station.Id == chatMember)
                    {
                        ChatSingleItemsRecolorer(chatMember, station.MessageFiled, startIndexOfList, blueColor, blackColor);
                    }

                    //if (station.IsSendByMe)
                    if (station.IsSendByMe == Roles.SentByMe)
                    {
                        if (stationsMessages.Count == 1 && station.Id == chatMember)
                        {
                            if (curIdOfStation != -1)
                            {
                                ChatSingleItemsRecolorer(curIdOfStation, blackColor, blackColor);
                                curIdOfStation = station.Id;
                            }
                            ChatSingleItemsRecolorer(chatMember, station.MessageFiled, startIndexOfList, grayColor, grayColor);
                        }
                        if (stationsMessages.Count != 1 && station.Id == chatMember)
                        {
                            ChatMultipleItemsRecolorer(chatMember, station);
                        }
                    }
                }
            }
        }

        private void ChatMultipleItemsRecolorer(int chatMemberId, Message station)
        {
            if (station.Id == curIdOfStation)
            {
                ChatSingleItemsRecolorer(station.Id, station.MessageFiled, startIndexOfList, grayColor, grayColor);
            }
            else
            {
                ChatSingleItemsRecolorer(station.Id, station.MessageFiled, startIndexOfList, blueColor, blackColor);
            }

        }

        private ChatListItemViewModel ChatItemFinder(int chatMemberId)
        {
            ChatListItemViewModel curChatItem = new ChatListItemViewModel();
            SideMenuItems.ToList().ForEach(item =>
            {
                if (item.Id == chatMemberId)
                    curChatItem = item;
            });
            return curChatItem;
        }

        #region All versions of "ChatSingleItemsRecolorer"
        /// <summary>
        /// Recolor single items and push it to positionToPush(we should set it)
        /// </summary>
        /// <param name="chatMemberId"></param>
        /// <param name="color"></param>
        private void ChatSingleItemsRecolorer(int chatMemberId, string stationMessage,
            int positionToPush, string newContentAvailableColor, string backgroundColor)
        {
            ChatListItemViewModel curChatItem = ChatItemFinder(chatMemberId);
            int curIndex = SideMenuItems.IndexOf(curChatItem);
            SideMenuItems.Move(curIndex, positionToPush);
            curChatItem.NewContentAvailableField = newContentAvailableColor;
            curChatItem.BackgrounField = backgroundColor;
            curChatItem.Message = stationMessage.Replace("\r\n", " ");

        }

        /// <summary>
        /// Recolor single items without pushing to another place
        /// </summary>
        /// <param name="chatMemberId"></param>
        /// <param name="newContentAvailableColor"></param>
        /// <param name="backgroundColor"></param>
        private void ChatSingleItemsRecolorer(int chatMemberId, string stationMessage,
            string newContentAvailableColor, string backgroundColor)
        {
            ChatListItemViewModel curChatItem = ChatItemFinder(chatMemberId);
            int curIndex = SideMenuItems.IndexOf(curChatItem);
            curChatItem.NewContentAvailableField = newContentAvailableColor;
            curChatItem.BackgrounField = backgroundColor;
            curChatItem.Message = stationMessage.Replace("\r\n", " ");

        }

        private void ChatSingleItemsRecolorer(int chatMemberId,
            string newContentAvailableColor, string backgroundColor)
        {
            ChatListItemViewModel curChatItem = ChatItemFinder(chatMemberId);
            int curIndex = SideMenuItems.IndexOf(curChatItem);
            curChatItem.NewContentAvailableField = newContentAvailableColor;
            curChatItem.BackgrounField = backgroundColor;

        }
        #endregion

        #endregion
        /// <summary>
        /// Method to delete from SideMenu by Id
        /// </summary>
        /// <param name="chatMember"></param>
        private void RemoveItems(List<int> listOfIdToDelete)
        {
            listOfIdToDelete.ForEach(chatMemberId =>
           {
               SideMenuItems.ToList().ForEach(item =>
               {
                   if (item.Id == chatMemberId)
                   {
                       SideMenuItems.Remove(item);
                       localListOfChatMembers.Remove(item.Id);
                   }
               });
           });
            Events.UpdateChatItems(localListOfChatMembers);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="listToAdd"></param>
        private void AddItems(List<StationClassForChat> listToAdd)
        {
            listToAdd.ForEach(chatMember =>
            {
                if (!localListOfChatMembers.Contains(chatMember.Id))
                {
                    SideMenuItems.Add(
                        new ChatListItemViewModel
                        {
                            Id = chatMember.Id,
                            Name = chatMember.Caption,
                            //Message = station.Value.Trim('\r','\n'),
                            ProfilePictureRGB = RandomRGBColor(),
                            //ProfilePictureRGB = RandomRGBСharacter(1) + RandomRGBСharacter(1) + RandomRGBСharacter(1) + RandomRGBСharacter(1) + RandomRGBСharacter(1) + RandomRGBСharacter(1),
                            NewContentAvailableField = blackColor,
                            BackgrounField = blackColor
                        });
                    localListOfChatMembers.Add(chatMember.Id);
                }
            });
            Events.UpdateChatItems(localListOfChatMembers);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="listToAdd"></param>
        private void UpdateItems(List<StationClassForChat> listToAdd)
        {
            //var curElement = listToAdd.ToList().Where(item => item.Id == curIdOfStation).FirstOrDefault();
            //if (curElement == null)
            //    Events.UserNotAviableAnymore();

            //remove them all because ASPTable update send whole list to us with each update 
            SideMenuItems.ToList().ForEach(item =>
            {
                //Save color of round
                if(!curColors.Keys.Contains(item.Id))
                curColors.Add(item.Id, item.ProfilePictureRGB);

                SideMenuItems.Remove(item);
                localListOfChatMembers.Remove(item.Id);
            });


            listToAdd.ForEach(chatMember =>
            {
                if (!localListOfChatMembers.Contains(chatMember.Id) && chatMember.Id!= curIdOfStation)
                {
                    SideMenuItems.Add(
                        new ChatListItemViewModel
                        {
                            Id = chatMember.Id,
                            Name = chatMember.Caption,
                            //Message = station.Value.Trim('\r','\n'),
                            //ProfilePictureRGB = RandomRGBColor(),
                            ProfilePictureRGB = curColors.Keys.Contains(chatMember.Id) ? curColors[chatMember.Id] : RandomRGBColor(),
                            //ProfilePictureRGB = RandomRGBСharacter(1) + RandomRGBСharacter(1) + RandomRGBСharacter(1) + RandomRGBСharacter(1) + RandomRGBСharacter(1) + RandomRGBСharacter(1),
                            NewContentAvailableField = blackColor,
                            BackgrounField = blackColor
                        });
                    localListOfChatMembers.Add(chatMember.Id);
                }
                else
                {
                    SideMenuItems.Add(
                       new ChatListItemViewModel
                       {
                           Id = chatMember.Id,
                           Name = chatMember.Caption,
                            //Message = station.Value.Trim('\r','\n'),
                            ProfilePictureRGB = RandomRGBColor(),
                            //ProfilePictureRGB = RandomRGBСharacter(1) + RandomRGBСharacter(1) + RandomRGBСharacter(1) + RandomRGBСharacter(1) + RandomRGBСharacter(1) + RandomRGBСharacter(1),
                            NewContentAvailableField = grayColor,
                           BackgrounField = grayColor
                       });
                    localListOfChatMembers.Add(chatMember.Id);
                }
            });
            Events.UpdateChatItems(localListOfChatMembers);
        }

        private string RandomRGBColor()
        {
            if (counterForRandomColor != 1)
            {
                string randomHexColor = curListOfColors[rm.Next(1, counterForRandomColor)];
                curListOfColors.Remove(randomHexColor);
                counterForRandomColor--;
                return randomHexColor;
            }
            else
            {
                string randomHexColor = RandomRGBСharacter(1) + RandomRGBСharacter(1) + RandomRGBСharacter(1) +
                    RandomRGBСharacter(1) + RandomRGBСharacter(1) + RandomRGBСharacter(1);
                return randomHexColor;
            }
        }


        private string RandomRGBСharacter(int length)
        {
            const string chars = "abcdef0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[rm.Next(s.Length)]).ToArray());
        }

        private void MessageUpdater(Dictionary<string, string> stationsMessages)
        {
            foreach (ChatListItemViewModel item in SideMenuItems)
            {
                if (stationsMessages.ContainsKey(item.Name))
                    item.Message = "sd";
            }
        }

    }
}
