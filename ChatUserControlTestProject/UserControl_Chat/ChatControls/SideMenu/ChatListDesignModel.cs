﻿
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace UserControl_Chat
{
    /// <summary>
    /// The design-time data for a <see cref="ChatListViewModel"/>
    /// </summary>
    public class ChatListDesignModel : ChatListViewModel
    {
        #region Singleton
        /// <summary>
        /// A single instance of the design model
        /// </summary>
        public static ChatListDesignModel Instance => new ChatListDesignModel();

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public ChatListDesignModel()
        {
            List<ChatListItemViewModel> SideMenuItems = new List<ChatListItemViewModel>
            {
                new ChatListItemViewModel
                {
                    Id = 90,
                    Name = "name_0",
                    //NewContentAvailable = true,
                    //IsSelected = true,
                    NewContentAvailableField = "333333",
                    BackgrounField = "333333",
                    ProfilePictureRGB = "fe4503",
                },
                new ChatListItemViewModel
                {
                    Id = 10,
                    Name = "name_10",
                    //NewContentAvailable = true,
                    //IsSelected = true,
                    NewContentAvailableField = "333333",
                    BackgrounField = "333333",
                    ProfilePictureRGB = "fe4503"
                },
                new ChatListItemViewModel
                {
                    Id = 20,
                    Name = "name_20",
                    //NewContentAvailable = true,
                    //IsSelected = true,
                    NewContentAvailableField = "333333",
                    BackgrounField = "333333",
                    ProfilePictureRGB = "00d405",
                },
                new ChatListItemViewModel
                {
                    Id = 50,
                    Name = "name_50",
                    //NewContentAvailable = true,
                    //IsSelected = true,
                    NewContentAvailableField = "333333",
                    BackgrounField = "333333",
                    ProfilePictureRGB = "3099c5"
                },
                
                new ChatListItemViewModel
                {
                   Id = 60,
                    Name = "name_0",
                    //NewContentAvailable = true,
                    //IsSelected = true,
                    NewContentAvailableField = "333333",
                    BackgrounField = "333333",
                    ProfilePictureRGB = "00d405"
                },
            };
        }

        #endregion
    }
}
