﻿using System;
using System.Collections.Generic;
using System.Windows;
using UserControl_Chat;

namespace ChatUserControlTestProject
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public List<StationClassForChat> curTestList;
        public List<Message> curStationsMessages;

        public MainWindow()
        {
            
            InitializeComponent();
            SetStations(); //TODO: Change it
            Events.OnSendStationsMessage += ShowOwnMessages;
            Events.OnSendStationsMessage += ReturnApprovedMessages;
            //UserControlChat1.SideMenuWidth = 25;
            //UserControlChat1.SetSideMenuWidth(50);
            //UserControlChat UserControlChat1 = new UserControlChat(250);
        }


        public void ShowOwnMessages(List<Message> stationsMessages)
        {
            curStationsMessages = stationsMessages;
            TextBloxWithId_Message.Text = String.Empty;
            foreach (Message stationMessage in stationsMessages)
            {
                TextBloxWithId_Message.Text += stationMessage.Id + " " + stationMessage.MessageFiled + "\r\n";
            }
        }

        public void ReturnApprovedMessages(List<Message> stationsMessages)
        {
            foreach (Message curStationsMessage in curStationsMessages)
            {
                //curStationsMessage.IsTransmited = true;
                curStationsMessage.SenderName = "Сергей Александрович";
                curStationsMessage.MessageFontSize = 20;
                curStationsMessage.SenderNameFontSize = 15;
                curStationsMessage.MessageFiled = curStationsMessage.MessageFiled;

                //curStationsMessage.IsTransmited = false;
            }
            UserControlChat1.DrawMessageToChat(stationsMessages);
        }


        private void Button_DeleteFewStations(object sender, RoutedEventArgs e)
        {
            int curId = string.IsNullOrEmpty(TextBoxWithId.Text.Substring(20)) ? 0 : Convert.ToInt32(TextBoxWithId.Text.Substring(20));

            List<int> testList = new List<int>();
            testList.Add(curId);
            testList.Add(curId+1);
            UserControlChat1.DeleteChatMembers(testList);
        }


        private void Button_AddFewStations(object sender, RoutedEventArgs e)
        {
            int curId = string.IsNullOrEmpty(TextBoxWithId.Text.Substring(20)) ? 0 : Convert.ToInt32(TextBoxWithId.Text.Substring(20));

            List<StationClassForChat> testList = new List<StationClassForChat>()
            {

                new StationClassForChat
                {
                    Id = curId,
                    Caption = "Caption;P",
                    IsActive = true
                },
                new StationClassForChat
                {
                    Id = curId+1,
                    Caption = "Caption;P",
                    IsActive = true
                }

        };
            UserControlChat1.AddChatMembers(testList);
        }


        private void Button_SendByFewStation(object sender, RoutedEventArgs e)
        {
            List<Message> stationsMessages = new List<Message>()
            {
                new Message()
                {
                    Id = 212,
                    MessageFiled = "Станция что-то передала",
                    IsSendByMe = UserControl_Chat.Roles.Received,
                    IsTransmited = true
                },
                new Message()
                {
                    Id = 213,
                    MessageFiled = "Станция что-то передала",
                    IsSendByMe = UserControl_Chat.Roles.Received,
                    IsTransmited = true
                },
                new Message()
                {
                    Id = 214,
                    MessageFiled = "Станция что-то передала",
                    IsSendByMe = UserControl_Chat.Roles.Received,
                    IsTransmited = true
                }
            };

            UserControlChat1.DrawMessageToChat(stationsMessages);
        }

        private void Button_SaveButton(object sender, RoutedEventArgs e)
        {
            Events.SaveChatStory();
        }

        private void Button_loadButton(object sender, RoutedEventArgs e)
        {
            Events.LoadChatStory();
        }

        private void SetStations()
        {
            List<StationClassForChat> testList = new List<StationClassForChat>()
            {
                new StationClassForChat(0,"ПУ", true),
                new StationClassForChat(212,"Caption1", true),
                new StationClassForChat(213,"Caption2", true),
                new StationClassForChat(214,"Caption3", true),
                new StationClassForChat(215,"Caption4", true),
            };
            UserControlChat1.InitStations(testList);
            curTestList = testList;

        }

        private void Button_Draw_As_OK_as_st(object sender, RoutedEventArgs e)
        {
            int curId = string.IsNullOrEmpty(TextBoxWithId.Text.Substring(20)) ? 0 : Convert.ToInt32(TextBoxWithId.Text.Substring(20));
            List<Message> stationsMessages = new List<Message>()
            {
                new Message()
                {
                    Id = curId,
                    MessageFiled = "Станция что-то передала",
                    IsSendByMe = UserControl_Chat.Roles.Received,
                    IsTransmited = true,
                    SenderNameFontSize = 25, //15,25,35...n5
                    ProfilePictureRGB ="FF33FF",
                    SenderName = "Иван Васильевич"
                    
                }
            };
            UserControlChat1.DrawMessageToChat(stationsMessages);
        }

        private void Draw_As_OK_as_user(object sender, RoutedEventArgs e)
        {
            List<Message> stationsMessages = new List<Message>()
            {
                new Message()
                {
                    Id = 0,
                    MessageFiled = "Пользователь отправил сообщение",
                    IsSendByMe = UserControl_Chat.Roles.SentByMe,
                    IsTransmited = true
                }
            };
            UserControlChat1.DrawMessageToChat(stationsMessages);
        }

        private void Draw_As_NOT_OK_as_user(object sender, RoutedEventArgs e)
        {
            List<Message> stationsMessages = new List<Message>()
            {
                new Message()
                {
                    Id = 0,
                    MessageFiled = "Сообщение пользователя не дошло",
                    IsSendByMe = UserControl_Chat.Roles.SentByMe,
                    IsTransmited = false
                }
            };
            UserControlChat1.DrawMessageToChat(stationsMessages);
        }


        private void Button_SendToAllAsUser(object sender, RoutedEventArgs e)
        {
            List<Message> stationsMessages = new List<Message>()
            {
             
            };
            foreach (StationClassForChat curStation in curTestList)
            {
                Message testName = new Message();
                testName.Id = curStation.Id;
                testName.MessageFiled = String.Format("Станция № {0} что-то передала", testName.Id);
                testName.IsSendByMe = UserControl_Chat.Roles.Received;
                testName.IsTransmited = false;
                stationsMessages.Add(testName);

                Message testName2 = new Message();
                testName2.Id = curStation.Id;
                testName2.MessageFiled = String.Format("Станция № {0} что-то передала", testName.Id);
                testName2.IsSendByMe = UserControl_Chat.Roles.SentByMe;
                testName2.IsTransmited = false;
                stationsMessages.Add(testName2);

                Message testName3 = new Message();
                testName3.Id = curStation.Id;
                testName3.MessageFiled = String.Format("Станция № {0} что-то передала", testName.Id);
                testName3.IsSendByMe = UserControl_Chat.Roles.SentByMe;
                testName3.IsTransmited = true;
                stationsMessages.Add(testName3);
            }
            UserControlChat1.DrawMessageToChat(stationsMessages);
        }

        private void Button_SendMessageAsAdmin(object sender, RoutedEventArgs e)
        {
            List<Message> stationsMessages = new List<Message>();

            Message testName = new Message();
            testName.Id = 0;
            testName.MessageFiled = "некое ВАЖНОЕ системное сообщение";
            testName.IsSendByMe = Roles.SendByAdmin;
            testName.IsTransmited = true;
            testName.MessageFontSize = 27;
            testName.HexColorOfMessageFont = "E51616";
            stationsMessages.Add(testName);

            UserControlChat1.DrawMessageToChat(stationsMessages);
        }
    }
}
